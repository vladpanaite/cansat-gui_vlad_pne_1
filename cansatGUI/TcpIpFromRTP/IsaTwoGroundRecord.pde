public class IsaTwoGroundRecord {
  public double roll, yaw, pitch;
  
  public IsaTwoGroundRecord(String line) {
    String[] data = line.split(",");
    roll = Double.parseDouble(data[37]);
    yaw = Double.parseDouble(data[38]);
    pitch = Double.parseDouble(data[39]);
  }
}
