import java.awt.*; 
import controlP5.*;
import java.net.*;
import java.io.*;
/******************* Global variables *******************/
ControlP5 cp5;
Knob altitudeKnob;
Knob tempKnob;
Knob pressureKnob;
Knob CO2Knob;

//Prevents pc from going to idle/screensaver
Robot robot;
int x, y, frCount= 0, prefix=-1;
Robot robby;

//Client myClient; 
Socket myClient;
String inString;
BufferedReader reader;
boolean ioException;
int numAvailable;
InputStream input;
boolean numberFormatException;
IsaTwoGUI gui= new IsaTwoGUI();

PVector a = new PVector(100, 50, 20);

String CSV_Line;
float roll;
float yaw;
float pitch;

float halfCan= Lc.canHeight/2;
float canWidth=0.2869565217391*Lc.canHeight;

float refWidth;
float refHeight;
float latitudeOnRef;
float longitudeOnRef;
float resolutionVKm;
float resolutionHKm;
float resolutionVDeg;
float resolutionHDeg;
float latitudeOrigin;
float longitudeOrigin;

float TL_latOnRef;
float TL_longOnRef;
float TR_latOnRef;
float TR_longOnRef;
float BR_latOnRef;
float BR_longOnRef;
float BL_latOnRef;
float BL_longOnRef;

PFont Arial;

PImage sky;


/******************* Settings *******************/
void settings() {
  fullScreen(P3D);
}

/******************* Setup *******************/
void setup()
{
  tcpSetup();

  robotSetup();

  refHeight= displayHeight/3;
  refWidth= displayWidth/3;

  perspective();

  chooseSite();

  imageSetup();

  knobsSetup();
}

/******************* Main sketch *******************/
void draw()
{
  robotDraw();

  tcpTryCatch();

  imageDraw();

  lights();

  fill(255, 128, 0);
  strokeWeight(1);

  draw3D();
  drawMap();

  setKnobsValue();
  int increment=30;
  int lineIdx=3;
  float textLength= textWidth("GPS FIX: ");
  
  strokeWeight(2);
 String tsCombine = "Timestamp: " + gui.timestamp;
  text(tsCombine, 10, increment*lineIdx++);
  text("GPS FIX: ", 10, increment*lineIdx);
  if (gui.fix== 1) {
    fill(0, 255, 0);
    ellipse(textLength+30, (increment*lineIdx++)-10, 30, 30);
    fill(0);
  } else {
    fill(255, 0, 0);
    ellipse(textLength+30, (increment*lineIdx++)-10, 30, 30);
    fill(0);
  }
  //printVariables();
  //delay(10000);
}

void knobsSetup() {
  cp5 = new ControlP5(this);
  Arial= createFont("Arial", 24);
  knobs(Arial, displayWidth/12);
}

void setKnobsValue() { 
  altitudeKnob.setValue(gui.altitude);
  tempKnob.setValue(gui.temperature);
  pressureKnob.setValue(gui.pressure);
  if (Lc.showCO2) {
    CO2Knob.setValue(gui.co2);
  }
}

void imageSetup() {
  if (Lc.imageBackground) {
    sky=loadImage("sky.png");
    sky.resize(displayWidth, displayHeight);
  }
}

void imageDraw() {
  if (Lc.imageBackground) {
    background(sky);
  } else {
    background(0, 128, 255);
  }
}

void chooseSite() {
  switch(Lc.sitePosition) {

  case PHYSICS_LAB:
    latitudeOrigin = Lc.latitudePhysicsLab; 
    longitudeOrigin = Lc.longitudePhysicsLab;
    Lc.verticalRange = 0.1;
    Lc.horizontalRange = 0.1  ;
    break;

  case COLLEGE:
    latitudeOrigin=Lc.latitudeCollege;
    longitudeOrigin=Lc.longitudeCollege;
    Lc.verticalRange = 3;
    Lc.horizontalRange = 3;
    break;

  case OPERATION_SITE:
    latitudeOrigin=Lc.latitudeOperation;
    longitudeOrigin=Lc.longitudeOperation;
    break;
  }
}

void tcpTryCatch() {
  try {
    if (myClient!=null) {      
      reader = new BufferedReader(new InputStreamReader(myClient.getInputStream()));
      inString = reader.readLine();
      numAvailable = reader.read();
      tcpDraw();
      numberFormatException=false;
    }
  } 
  catch(NumberFormatException e) {
    System.out.println("Something went wrong in the parsing, exiting draw."); 
    System.out.println("Waiting for next record...");
    System.out.println(e.getMessage());
    numberFormatException=true;
    return;
  } 
  catch(IOException e) {
    System.out.println("IOException");
    System.out.println(e.getMessage());
    return;
  }
  finally {
    if (numberFormatException) {
      System.out.println("Exited loop (NumberFormatException)");
    }
  }
}

void robotSetup() {
  try {
    robot = new Robot();
  }
  catch (AWTException e) {
    return;
  }
}

void robotDraw() {
  if (frCount == 300) {
    x = mouseX;
    y = mouseY;
    x = (x + 40*(prefix));
    robot.mouseMove( x, y );
    prefix = -1*prefix;
    frCount = 0;
  }
  frCount = frCount + 1;
}

// Draw the complete 3D representation. Orientation is preserved by a push/pop pair. 
void draw3D() {
  pushMatrix();
  translate( displayWidth*0.55, displayHeight/4, 0 );

  /* BDH: 
   * We want to be X=back, Y = right, Z = down in a right-handed referential.
   * Since Processing3 referential is left-handed (cf. https://processing.org/tutorials/p3d/),
   * our Z will be -Z in Processing.
   * Our strategy is consequently to obtain: X=back, Y = right, Z_processing = Up.
   *   using 2 initial rotations: rotateX(+PI/2) then rotateZ(-PI/2). 
   * The sign of Z coordinates will then be changed when drawing.
   * Regarding the rotations: positive rotations are:
   *    Around Z: from X to Y. This is Euler's Yaw: OK.
   *    Around Y: from X to Z_processing = Z to X. This is the opposite of Euler's pitch.
   *    Around X: from Y to Z_processing = from Z to Y. This is the opposite of Euler's roll.
   * Applying the Euler's angles is consequently:
   *    rotateZ(Yaw) + rotateY(-pitch) + rotateX(-roll). 
   */
  rotateX(PI/2);
  rotateZ(-PI/2);
  drawCan3D();
  drawInertialReferential();
  popMatrix();

  if ( Lc.axisLength<(halfCan + Lc.pyramidHeight)) {
    fill(192, 0, 0);
    textSize(30);
    textAlign(CENTER);
    text("Warning: axisLength too short to be visible", displayWidth/2, (displayHeight/2)- 200);
  }
}

// Draw inertial referential: context is preserved in a push-pop pair
void drawInertialReferential() {
  pushMatrix();
  translate(0, 300, 0);  
  drawAxes(100);
  popMatrix();
}

// Draw the can in the current.
// Orientation is preserved in a push-pop pair. 
void drawCan3D() {
  pushMatrix();
  //Draw can and vision cone

  // Apply Euler angles (see note in draw3D about the signs.  // BDH
  if (Lc.rotateCan) {
    rotateZ( gui.yaw );
    rotateY( -gui.pitch ); // BDH
    rotateX( -gui.roll );  // BDH
  }
  strokeWeight(1);
  stroke(0, 0, 0);
  if (Lc.drawCan) {
    drawCylinder( 40, canWidth, Lc.canHeight );
    // Vision pyramid
    strokeWeight(0.5);
    fill(200);
    drawPyramid(25, 40, 150, 100, Lc.pyramidHeight);
  }
  if ( Lc.drawAxisOnCan && Lc.axisLength>=(halfCan + Lc.pyramidHeight)) {
    drawAxes(Lc.axisLength);
  }

  if (Lc.showRawMagField) {
    // show magnetic field and gravitation
    color c =color(153, 51, 255); // dark blue
    float rawMagnitude = sqrt(gui.rmX*gui.rmX + gui.rmY*gui.rmY + gui.rmZ*gui.rmZ);
    drawDirection(gui.rmX, gui.rmY, gui.rmZ, "", rawMagnitude/80, c, 2); // Normalize raw mag field.
  }
  if (Lc.showMagField) {
    color c =color(153, 51, 255); // dark blue
    float magnitude = sqrt(gui.cmX*gui.cmX + gui.cmY*gui.cmY + gui.cmZ*gui.cmZ);
    drawDirection(gui.cmX, gui.cmY, gui.cmZ, "M", magnitude/150, c, 2);
  }
  if (Lc.showGravity) {
    color c =color(102, 51, 0); // brown
    drawDirection(gui.caX, gui.caY, gui.caZ, "G", Lc.gravityDivider, c, 5);
  }

  popMatrix();
}

// Visualise a direction (from origin to (dx,dy,dz).
void drawDirection(float dx, float dy, float dz, String label, float divider, color c, int weight) {
  pushMatrix();
  strokeWeight(weight);
  stroke(c);
  fill(c);
  line(0, 0, 0, dx/divider, dy/divider, dz/divider);
  strokeWeight(1);
  line(dx/divider, dy/divider, 0, dx/divider, dy/divider, dz/divider);
  line( 0, dy/divider, dz/divider, dx/divider, dy/divider, dz/divider);
  line(dx/divider, 0, dz/divider, dx/divider, dy/divider, dz/divider);
  line(dx/divider, dy/divider, 0, dx/divider, 0, 0);
  line(dx/divider, dy/divider, 0, 0, dy/divider, 0);
  line( 0, dy/divider, dz/divider, 0, dy/divider, 0);
  line( 0, dy/divider, dz/divider, 0, 0, dz/divider);
  line(dx/divider, 0, dz/divider, 0, 0, dz/divider);
  line(dx/divider, 0, dz/divider, dx/divider, 0, 0);
  translate(dx/divider, dy/divider, dz/divider);
  textSize(30);
  text(label, 0, 0);
  popMatrix();
}

void knobs(PFont font, float radius) {
  altitudeKnob = cp5.addKnob("Altitude (m)")
    .setRange(-100, 2000)
    .setValue(100)
    .setPosition(20, displayHeight-2*radius-40)
    .setRadius(radius)
    .setNumberOfTickMarks(21)
    .setTickMarkLength(15)
    .snapToTickMarks(false)
    .setColorForeground(color(255))
    .setColorBackground(color(0, 50, 200))
    .setColorActive(color(255, 255, 0))
    .setFont(font);
  ;

  tempKnob = cp5.addKnob("Temperature (°C)")
    .setRange(0, 30)
    .setValue(0)
    .setPosition(80+2*radius, displayHeight-2*radius-40)
    .setRadius(radius)
    .setNumberOfTickMarks(6)
    .setTickMarkLength(15)
    .snapToTickMarks(false)
    .setColorForeground(color(255))
    .setColorBackground(color(0, 50, 200))
    .setColorActive(color(255, 255, 0))
    .setFont(font);
  ;

  pressureKnob = cp5.addKnob("Pressure (hPa)")
    .setRange(800, 1200)
    .setValue(0)
    .setPosition(155+4*radius, displayHeight-2*radius-40)
    .setRadius(radius)
    .setNumberOfTickMarks(16)
    .setTickMarkLength(15)
    .snapToTickMarks(false)
    .setColorForeground(color(255))
    .setColorBackground(color(0, 50, 200))
    .setColorActive(color(255, 255, 0))
    .setFont(font);
  ;
  if (Lc.showCO2) {
    CO2Knob = cp5.addKnob("CO2 concentration (PPM)")
      .setRange(0, 8000)
      .setValue(0)
      .setPosition(displayWidth-2*radius-40, displayHeight-2*radius- refHeight-40)
      .setRadius(radius)
      .setNumberOfTickMarks(16)
      .setTickMarkLength(15)
      .snapToTickMarks(false)
      .setColorForeground(color(255))
      .setColorBackground(color(0, 50, 200))
      .setColorActive(color(255, 255, 0))
      .setFont(font);
    ;
  }
}

void drawCylinder( int sides, float r, float h)
{
  float angle = 360 / sides;
  float halfHeight = h / 2;

  // draw top of the tube
  beginShape();
  strokeWeight(2);
  for (int i = 0; i < sides; i++) {
    float x = cos( radians( i * angle ) ) * r;
    float y = sin( radians( i * angle ) ) * r;
    vertex( x, y, -halfHeight);
  }
  endShape(CLOSE);

  // draw bottom of the tube
  beginShape();
  strokeWeight(2);
  for (int i = 0; i < sides; i++) {
    float x = cos( radians( i * angle ) ) * r;
    float y = sin( radians( i * angle ) ) * r;
    vertex( x, y, halfHeight);
  }
  endShape(CLOSE);

  // draw sides
  beginShape(TRIANGLE_STRIP);
  noStroke();
  for (int i = 0; i < sides + 1; i++) {
    float x = cos( radians( i * angle ) ) * r;
    float y = sin( radians( i * angle ) ) * r;
    vertex( x, y, halfHeight);
    vertex( x, y, -halfHeight);
  }
  endShape(CLOSE);
}

void drawLine (int x1, int y1, int x2, int y2, int lineWidth) {
  strokeWeight(lineWidth);
  line(x1, y1, x2, y2);
}

void drawPyramid(float lTop, float LTop, float lBottom, float LBottom, float h)
{
  pushMatrix();
  strokeWeight(2);
  stroke(0);

  float halfHeight= h/2;
  float halfCan= Lc.canHeight/2;

  translate(0, 0, -(halfCan+halfHeight+1)); // BDH

  //Draw top rectangle
  beginShape();
  float hlTop=lTop/2;
  float hLTop=LTop/2;
  vertex(hlTop, hLTop, halfHeight); // BDH
  vertex(hlTop, -hLTop, halfHeight); // BDH
  vertex(-hlTop, -hLTop, halfHeight); // BDH
  vertex(-hlTop, hLTop, halfHeight); // BDH
  endShape(CLOSE);

  //Draw bottom rectangle
  beginShape();
  float hlBottom=lBottom/2;
  float hLBottom=LBottom/2;
  noFill();
  vertex(hlBottom, hLBottom, -halfHeight); // BDH
  vertex(hlBottom, -hLBottom, -halfHeight); // BDH
  vertex(-hlBottom, -hLBottom, -halfHeight); // BDH
  vertex(-hlBottom, hLBottom, -halfHeight); // BDH
  endShape(CLOSE);

  //Draw sides
  beginShape(QUADS);
  fill(150);
  vertex(hlTop, hLTop, halfHeight); // BDH
  vertex(hlTop, -hLTop, halfHeight); // BDH
  vertex(hlBottom, -hLBottom, -halfHeight); // BDH
  vertex(hlBottom, hLBottom, -halfHeight); // BDH

  vertex(hlTop, -hLTop, halfHeight); // BDH
  vertex(-hlTop, -hLTop, halfHeight); // BDH
  vertex(-hlBottom, -hLBottom, -halfHeight); // BDH
  vertex(hlBottom, -hLBottom, -halfHeight); // BDH

  vertex(-hlTop, hLTop, halfHeight); // BDH
  vertex(-hlTop, -hLTop, halfHeight); // BDH
  vertex(-hlBottom, -hLBottom, -halfHeight); // BDH
  vertex(-hlBottom, hLBottom, -halfHeight);  // BDH

  vertex(hlTop, hLTop, halfHeight); // BDH
  vertex(-hlTop, hLTop, halfHeight); // BDH
  vertex(-hlBottom, hLBottom, -halfHeight); // BDH
  vertex(hlBottom, hLBottom, -halfHeight);  // BDH
  endShape();
  popMatrix();
}

void tcpSetup() {
  println("Attempting to connect to "+Lc.address+":" + Lc.portNumber);
  try {
    myClient = new Socket(Lc.address, Lc.portNumber);
  } 
  catch(IOException e) {
    System.out.println("TCP setup IOException: " + e.getMessage());
  }
  println("Setup ok");
}

void tcpDraw() {
  try {
    if (myClient!=null && numAvailable!=-1) {
      background(0);

      //drop final end-of-line
      if (inString != null && inString.length() > 0) {
        inString=inString.replace("\n", "");
      }
      gui.update(inString);
    } else {
      println("Connecting....");
      numAvailable =  reader.read();
      myClient= new Socket(Lc.address, Lc.portNumber);
      if (numAvailable == -1) {
        println("*** Error connecting to "+Lc.address+ ":" + Lc.portNumber);
        println("*** Is there a server listening on this port at this address?");
      } else {
        println("Connected to "+Lc.address+ ":" + Lc.portNumber);
      }
    }
  }
  catch(SocketTimeoutException s) {
    System.out.println("SocketTimeoutException" + s.getMessage());
    return;
  }
  catch(IOException e) {
    System.out.println(e.getMessage());
    System.out.println("Connexion failed, retrying...");
    return;
  }
  finally {
    if (ioException) {
      System.out.println("Exited loop (IOException 2)");
    }
  }
}
// Draw cartesian referential in current context. No translation nor rotation applied.
// Remember our Z-axis is opposed to Processing's one: we use a right-handed referential
// while Processing's is left-handed.  (BDH)
void drawAxes(float size) {
  pushMatrix();
  textFont(Arial, 24);

  //X  - red
  stroke(192, 0, 0);
  fill(192, 0, 0);
  strokeWeight(3);
  line(0, 0, 0, size, 0, 0);
  strokeWeight(1);
  line(0, 0, 0, -size, 0, 0);
  pushMatrix();
  translate(size, 0, 0);
  pushMatrix();
  rotateY(-PI/2);
  drawCone(40, 0, 8, 16);
  popMatrix();
  rotateX(PI/2);  // Rotate text to have it in XZ plane
  rotateY(-PI/2);
  text(" X", 0, 0, 0);
  popMatrix();

  //Y - green
  stroke(0, 192, 0); 
  fill(0, 192, 0); 
  strokeWeight(3);
  line(0, 0, 0, 0, size, 0);
  strokeWeight(1);
  line(0, 0, 0, 0, -size, 0);
  pushMatrix();
  translate(0, size, 0);
  pushMatrix();
  rotateX(PI/2);
  drawCone(40, 0, 8, 16);
  popMatrix();
  // Rotate text to have it in right plane
  rotateY(PI/2);
  rotateZ(-PI/2);
  rotateY(PI);
  textFont(Arial, 24);
  text(" Y", 0, 0, 0);
  popMatrix();

  //Z - blue
  stroke(0, 0, 192);
  fill(0, 0, 192);
  strokeWeight(3);
  line(0, 0, 0, 0, 0, -size); //BDH
  strokeWeight(1);
  line(0, 0, 0, 0, 0, size); //BDH
  pushMatrix();
  translate(0, 0, -size);
  drawCone(40, 0, 8, 16);
  rotateX(PI/2);  // Rotate text to have it in right plane
  rotateY(PI/2);
  rotateX(PI);
  text(" Z", 0, 0, 0);
  popMatrix();

  popMatrix();
}

void drawCone( int sides, float rTop, float rBottom, float h)
{
  float angle = 360 / sides;
  float halfHeight = h / 2;

  // draw top of the cone
  beginShape();
  strokeWeight(2);
  for (int i = 0; i < sides; i++) {
    float x = cos( radians( i * angle ) ) * rTop;
    float y = sin( radians( i * angle ) ) * rTop;
    vertex( x, y, -halfHeight);
  }
  endShape(CLOSE);

  // draw bottom of the cone
  beginShape();
  strokeWeight(2);
  for (int i = 0; i < sides; i++) {
    float x = cos( radians( i * angle ) ) * rBottom;
    float y = sin( radians( i * angle ) ) * rBottom;
    vertex( x, y, halfHeight);
  }
  endShape(CLOSE);

  // draw sides
  beginShape(TRIANGLE_STRIP);
  noStroke();
  for (int i = 0; i < sides + 1; i++) {
    float x = cos( radians( i * angle ) ) * rTop;
    float y = sin( radians( i * angle ) ) * rTop;
    float x2 = cos( radians( i * angle ) ) * rBottom;
    float y2 = sin( radians( i * angle ) ) * rBottom;
    vertex( x2, y2, halfHeight);
    vertex( x, y, -halfHeight);
  }
  endShape(CLOSE);
}

void drawArrow(int cx, int cy, int len, float angle) {
  translate(cx, cy);
  rotate(angle);
  stroke(0, 0, 0);
  line(0, 0, len, 0);
  line(len, 0, len - 8, -8);
  line(len, 0, len - 8, 8);
}

void drawXAxis () {
  stroke(0, 0, 0);
  line(-(refWidth/2), 0, refWidth/2, 0);
  line(refWidth/2, 0, (refWidth/2)-8, -8);
  line(refWidth/2, 0, (refWidth/2)-8, 8);
}

void drawYAxis () {
  stroke(0, 0, 0);
  line(0, refHeight/2, 0, -(refHeight/2));
  line(0, -(refHeight/2), -8, -(refHeight/2)+8);
  line(0, -(refHeight/2), 8, -(refHeight/2)+8);
}

void drawMap() {  
  float resolutionV_PixelsPerKm = refHeight/(2*Lc.verticalRange);
  float resolutionH_PixelsPerKm = refWidth/(2*Lc.horizontalRange);

  float resolutionV_PixelsPerDeg = resolutionV_PixelsPerKm*111.238;  // 111.111 km/lat degree.
  float resolutionH_PixelsPerDeg = resolutionH_PixelsPerKm*(111.238*cos(radians(latitudeOrigin)));

  // TMP For debugging: position can at (range/2;range/2)
  // gui.longitude = longitudeOrigin+ (horizontalRange/2.0*resolutionH_PixelsPerKm/resolutionH_PixelsPerDeg);
  // gui.latitude  = latitudeOrigin+ (verticalRange/2.0*resolutionV_PixelsPerKm/resolutionV_PixelsPerDeg);

  float canCoordX = ((gui.longitude-longitudeOrigin)*resolutionH_PixelsPerDeg);
  float canCoordY = -((gui.latitude-latitudeOrigin)*resolutionV_PixelsPerDeg);

  //Draw point representing the can's position + referential
  pushMatrix();

  // Draw map structure.
  rectMode(CENTER);
  translate((2*refWidth)+(refWidth/2), (2*refHeight)+(refHeight/2));
  //translate(refWidth/2, refHeight/2);
  fill(255); 
  rect(0, 0, refWidth, -refHeight);
  strokeWeight(3);
  drawXAxis ();
  drawYAxis ();

  // Draw viewed quadrilateral
  strokeWeight(3);
  //gui.BR_long=4.34019;
  TL_longOnRef=(gui.TL_long-longitudeOrigin)*resolutionH_PixelsPerDeg;
  TL_latOnRef=-((gui.TL_lat-latitudeOrigin)*resolutionV_PixelsPerDeg);

  TR_longOnRef=(gui.TR_long-longitudeOrigin)*resolutionH_PixelsPerDeg;
  TR_latOnRef=-((gui.TR_lat-latitudeOrigin)*resolutionV_PixelsPerDeg);

  BR_longOnRef=(gui.BR_long-longitudeOrigin)*resolutionH_PixelsPerDeg;
  BR_latOnRef=-((gui.BR_lat-latitudeOrigin)*resolutionV_PixelsPerDeg);

  BL_longOnRef=(gui.BL_long-longitudeOrigin)*resolutionH_PixelsPerDeg;
  BL_latOnRef=-((gui.BL_lat-latitudeOrigin)*resolutionV_PixelsPerDeg);

  if ((gui.roll<(PI/4) && gui.roll>(-PI/4)) && (gui.pitch<(PI/4) && gui.pitch>(-PI/4))) {

    beginShape(QUAD);
    fill(color(175));
    // TMP for debugging
    //gui.TL_long=longitudeOrigin+ (horizontalRange/2.0*resolutionH_PixelsPerKm/resolutionH_PixelsPerDeg);
    //gui.TL_lat= latitudeOrigin+ (verticalRange/2.0*resolutionV_PixelsPerKm/resolutionV_PixelsPerDeg);
    //gui.TR_long=longitudeOrigin- (horizontalRange/2.0*resolutionH_PixelsPerKm/resolutionH_PixelsPerDeg);
    //gui.TR_lat= latitudeOrigin+ (verticalRange/2.0*resolutionV_PixelsPerKm/resolutionV_PixelsPerDeg);
    //gui.BL_long=longitudeOrigin+ (horizontalRange/2.0*resolutionH_PixelsPerKm/resolutionH_PixelsPerDeg);
    //gui.BL_lat= latitudeOrigin- (verticalRange/2.0*resolutionV_PixelsPerKm/resolutionV_PixelsPerDeg);
    //gui.BR_long=longitudeOrigin- (horizontalRange/2.0*resolutionH_PixelsPerKm/resolutionH_PixelsPerDeg);
    //gui.BR_lat= latitudeOrigin- (verticalRange/2.0*resolutionV_PixelsPerKm/resolutionV_PixelsPerDeg);

    vertex(TL_longOnRef, TL_latOnRef);
    vertex(TR_longOnRef, TR_latOnRef);
    vertex(BR_longOnRef, BR_latOnRef);
    vertex(BL_longOnRef, BL_latOnRef);

    clip(displayWidth-refWidth, displayHeight-refHeight, refWidth, refHeight);
    endShape(CLOSE);
  }
  noClip();
  //text("TL", (gui.TL_long-longitudeOrigin)*resolutionH_PixelsPerDeg, -((gui.TL_lat-latitudeOrigin)*resolutionV_PixelsPerDeg));
  //text("TR", (gui.TR_long-longitudeOrigin)*resolutionH_PixelsPerDeg, -((gui.TR_lat-latitudeOrigin)*resolutionV_PixelsPerDeg));
  //text("BR", (gui.BR_long-longitudeOrigin)*resolutionH_PixelsPerDeg, -((gui.BR_lat-latitudeOrigin)*resolutionV_PixelsPerDeg));
  //text("BL", (gui.BL_long-longitudeOrigin)*resolutionH_PixelsPerDeg, -((gui.BL_lat-latitudeOrigin)*resolutionV_PixelsPerDeg));

  // Can position.
  strokeWeight(7);
  point(canCoordX, canCoordY);
  popMatrix();
}

void printVariables() {

  String tsCombine = "Timestamp: " + gui.timestamp;
  String rollCombine = String.format("Roll: %2.2f rad (%.0f°)", gui.roll, gui.roll*180/PI);
  String yawCombine = String.format("Yaw: %2.2f rad (%.0f°)", gui.yaw, gui.yaw*180/PI);
  String pitchCombine = String.format("Pitch: %2.2f rad (%.0f°)", gui.pitch, gui.pitch*180/PI);
  String pressureCombine = "Pressure: " + gui.pressure;
  String temperatureCombine = "Temperature; " + gui.temperature;
  String altitudeCombine = "Altitude: " + gui.altitude;
  String co2Combine = "CO2 Concentration: " + gui.co2;
  String xPosCombine = "X position: " + gui.xPos;
  String yPosCombine = "Y position: " + gui.yPos;
  String zPosCombine = "Z position: " + gui.zPos;
  String latitudeCombine = "Latitude: " + gui.latitude;
  String longitudeCombine= "Longitude: " + gui.longitude;
  float textLength= textWidth("GPS FIX: ");
  pushMatrix();
  textAlign(LEFT);
  textSize(25);
  fill(0);
  strokeWeight(2);
  int increment=30;
  int lineIdx=3;
  text(tsCombine, 10, increment*lineIdx++);
  text("GPS FIX: ", 10, increment*lineIdx);
  if (gui.fix== 1) {
    fill(0, 255, 0);
    ellipse(textLength+30, (increment*lineIdx++)-10, 30, 30);
    fill(0);
  } else {
    fill(255, 0, 0);
    ellipse(textLength+30, (increment*lineIdx++)-10, 30, 30);
    fill(0);
  }
  text(rollCombine, 10, increment*lineIdx++);
  text(yawCombine, 10, increment*lineIdx++);
  text(pitchCombine, 10, increment*lineIdx++);
  //text(pressureCombine, 10, increment*lineIndx++);
  //text(temperatureCombine, 10, increment*lineIndx++);
  //text(altitudeCombine, 10, increment*lineIndx++);
  //text(co2Combine, 10, increment*lineIdx++);
  text(xPosCombine, 10, increment*lineIdx++);
  text(yPosCombine, 10, increment*lineIdx++);
  text(zPosCombine, 10, increment*lineIdx++);
  text(latitudeCombine, 10, increment*lineIdx++);
  text(longitudeCombine, 10, increment*lineIdx++);
  text("Top left rect: " + TL_longOnRef + " / " + TL_latOnRef, 10, increment*lineIdx++);
  text("Top right rect: " + TR_longOnRef + " / " + TR_latOnRef, 10, increment*lineIdx++);
  text("Bottom right rect: " + BR_longOnRef + " / " + BR_latOnRef, 10, increment*lineIdx++);
  text("Bottom left rect: " + BL_longOnRef + " / " + BL_latOnRef, 10, increment*lineIdx++);
  text("Top left deg: " + gui.TL_long + " / " + gui.TL_lat, 10, increment*lineIdx++);
  text("Top right deg: " + gui.TR_long + " / " + gui.TR_lat, 10, increment*lineIdx++);
  text("Bottom left deg: " + gui.BL_long + " / " + gui.BL_lat, 10, increment*lineIdx++);
  text("Bottom right deg: " + gui.BR_long + " / " + gui.BR_lat, 10, increment*lineIdx++);

  text(TL_longOnRef, 10, increment*lineIdx++);
  text("Altitude: GPS/GPS corrected: " + gui.altitude_GPS + " / " + gui.altitude_GPScorr, 10, increment*lineIdx++);
  text("Debug for dev: " + gui.temperature, 10, increment*lineIdx++);

  if (Lc.printIMU_Values) {
    translate(0, increment*lineIdx);
    lineIdx=0;
    increment=20;
    textSize(16);
    String ca=String.format("Calib accel. (m/s^2): %6.2f  %6.2f  %6.2f", gui.caX, gui.caY, gui.caZ);
    String cg=String.format("Calib gyro   (dps)  : %6.2f  %6.2f  %6.2f", gui.cgX, gui.cgY, gui.cgZ);
    String cm=String.format("Calib mag    (µT)   : %6.2f  %6.2f  %6.2f", gui.cmX, gui.cmY, gui.cmZ);
    String ra=String.format("Raw accel.  : %5.0f  %5.0f  %5.0f", gui.raX, gui.raY, gui.raZ);
    String magG=" Magnitude : "+ sqrt(gui.caX*gui.caX + gui.caY*gui.caY + gui.caZ*gui.caZ);
    String rg=String.format("Raw gyro    : %5.0f  %5.0f  %5.0f", gui.rgX, gui.rgY, gui.rgZ);
    String rm=String.format("Raw mag (µT): %6.2f  %6.2f  %6.2f", gui.rmX, gui.rmY, gui.rmZ);
    String magInt=" Magnitude : "+ sqrt(gui.cmX*gui.cmX + gui.cmY*gui.cmY + gui.cmZ*gui.cmZ);
    text(ra, 10, increment*lineIdx++);
    text(rg, 10, increment*lineIdx++);
    text(rm, 10, increment*lineIdx++);
    text(ca+magG, 10, increment*lineIdx++);
    text(cg, 10, increment*lineIdx++);
    text(cm+magInt, 10, increment*lineIdx++);
  }
  popMatrix();
}
